from flask_restplus import Model, fields

new_contact_schema = Model('Contacts', {
    "firstName": fields.String(),
    "lastName": fields.String(),
    "email": fields.String(),
    "phoneNumber": fields.String(),
    "phoneExt": fields.String(),
    "contactType": fields.String()
})

