from flask_restplus import Model, fields

new_address_schema = Model('Address', {
    "address1": fields.String(),
    "address2": fields.String(),
    "address3": fields.String(),
    "city": fields.String(),
    "state": fields.String(),
    "postalCode": fields.String(),
    "country": fields.String()
})