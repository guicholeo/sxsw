from flask_restplus import Model, fields
from services.common.v1.schemas import contact, subscription, payment
from services.common.v1.schemas.contact import new_contact_schema
from services.common.v1.schemas.subscription import new_subscription_schema
from services.common.v1.schemas.payment import new_payment_schema

new_account_schema = Model('Account', {
    "email" : fields.String(),
    "companyName": fields.String(),
    "address1": fields.String(),
    "address2": fields.String(),
    "city": fields.String(),
    "state": fields.String(),
    "postCode": fields.String(),
    "country": fields.String(),
    "type": fields.String(),
    "active": fields.String(),
    # "contacts": fields.List(fields.Nested(new_contact_schema)),
    # "paymentToken": fields.Nested(new_payment_schema),
    # "subscription": fields.Nested(new_subscription_schema)


#   JSON REFERENCE

    # "contacts": [
    #     {
    #         "firstName": fields.String(),
    #         "lastName": fields.String(),
    #         "Email": fields.String(),
    #         "Phone": fields.String(),
    #         "PhoneExt": fields.String(),
    #         "Type": "Technical"
    #     },
    #     {
    #         "FirstName": fields.String(),
    #         "LastName": fields.String(),
    #         "Email": fields.String(),
    #         "Phone": fields.String(),
    #         "PhoneExt": fields.String(),
    #         "Type": "Billing"
    #     }],
    # "PaymentToken":
    #     {
    #         "CardToken": fields.String(),
    #         "CardHolderName": fields.String(),
    #         "CardType": fields.String(),
    #         "CardExpirationDate": fields.DateTime()
    #     },
    # "Subscription":
    #     {
    #         "Plan": fields.String(),
    #         "StartDate": fields.DateTime(),
    #         "EndDate": fields.DateTime(),
    #         "AutoRenew": fields.Boolean(),
    #         "PlanRate": fields.String(),
    #         "PlanDiscount": fields.String(),
    #         "DiscountEndDate": fields.DateTime()
    #     }


})