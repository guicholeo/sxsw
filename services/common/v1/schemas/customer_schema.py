from flask_restplus import Model, fields
from services.common.v1.schemas.contact import new_contact_schema
from services.common.v1.schemas.subscription import new_subscription_schema
from services.common.v1.schemas.address import new_address_schema


new_customer_schema = Model('Customer', {
    #"account_number": fields.String(),
    "email": fields.String(),
    "password": fields.String(),
    "companyName": fields.String(),
    "type": fields.String(),
    "customerInformation": fields.Nested(new_contact_schema),
    'address': fields.Nested(new_address_schema),
    "contacts": fields.List(fields.Nested(new_contact_schema)),
    "subscription": fields.List(fields.Nested(new_subscription_schema))
})
