from flask_restplus import Model, fields

new_subscription_schema = Model('Subscription', {
    "Plan": fields.String(),
    "StartDate": fields.DateTime(),
    "EndDate": fields.DateTime(),
    "AutoRenew": fields.Boolean(),
    "PlanRate": fields.String(),
    "PlanDiscount": fields.String(),
    "DiscountEndDate": fields.DateTime(),
    "ProductType" : fields.String()
})