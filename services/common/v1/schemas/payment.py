from flask_restplus import Model, fields

new_payment_schema = Model('Payment', {
    "CardToken": fields.String(),
    "CardHolderName": fields.String(),
    "CardType": fields.String(),
    "CardExpirationDate": fields.DateTime()
})