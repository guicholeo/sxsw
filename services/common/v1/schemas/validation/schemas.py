from schema import Schema,Optional,Use

#schemas used to valide that json data. Different than the API schemas. 
#another library that will let me choose which to make optional? make my own?
register_customer_schema = Schema({
  "email": Use(str),
  "password": Use(str),
  "companyName": Use(str),
  "type": Use(str),
  "customerInformation": {
    "firstName": Use(str),
    "lastName": Use(str),
    "email": Use(str),
    "phoneNumber": Use(str),
    Optional("phoneExt"): Use(str),
    Optional("contactType"): Use(str)
  },
  "address": {
    "address1": Use(str),
    Optional("address2"): Use(str),
    Optional("address3"): Use(str),
    "city": Use(str),
    "state":Use(str),
    "postalCode":Use(str),
    "country":Use(str)
  },
  Optional("contacts"): [{
    Optional("firstName"): Use(str),
    Optional("lastName"): Use(str),
    Optional("email"): Use(str),
    Optional("phoneNumber"): Use(str),
    Optional("phoneExt"): Use(str),
    Optional("contactType"): Use(str)
  }],
  "subscription": [{
    "Plan": Use(str),
    "StartDate": Use(str),
    "EndDate": Use(str),
    "AutoRenew": Use(str),
    "PlanRate": Use(str),
    "PlanDiscount": Use(str),
    "DiscountEndDate": Use(str),
    "ProductType": Use(str)
  }]
})

update_customer_schema = Schema({
  Optional("email"): Use(str),
  Optional("companyName"): Use(str),
  Optional("type"): Use(str),
  Optional("address"): {
    Optional("address1"): Use(str),
    Optional("address2"): Use(str),
    Optional("address3"): Use(str),
    Optional("city"): Use(str),
    Optional("state"):Use(str),
    Optional("postalCode"):Use(str),
    Optional("country"):Use(str)
  },
  Optional("customerInformation"): [{
    Optional("firstName"): Use(str),
    Optional("lastName"): Use(str),
    Optional("email"): Use(str),
    Optional("phoneNumber"): Use(str),
    Optional("phoneExt"): Use(str),
    Optional("contactType"): Use(str)
  }]
})

subscription_update_schema = Schema({
  "Plan": Use(str),
  "StartDate": Use(str),
  "EndDate": Use(str),
  "AutoRenew": Use(str),
  "PlanRate": Use(str),
  "PlanDiscount": Use(str),
  "DiscountEndDate": Use(str),
  "ProductType": Use(str)
})

