def createResponseBody(status,error=None,body=None):
	response = {
		"error" : error,
		"status_code": status,
		"body" :body
	}
	return response