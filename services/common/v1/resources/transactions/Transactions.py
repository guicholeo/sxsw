import logging, squareconnect, uuid,string,random,ccy,json
from flask_restplus import abort
from flask_mail import Mail, Message
import datetime
from flask import render_template
from decimal import Decimal
from squareconnect.rest import ApiException
from squareconnect.apis.locations_api import LocationsApi
from squareconnect.apis.transactions_api import TransactionsApi
from squareconnect.models.charge_request import ChargeRequest
from squareconnect.models.create_refund_request import CreateRefundRequest
from squareconnect.models.address import Address
from squareconnect.models.money import Money
from services.extensions import dynamo,mail,logger
from services.config import RequiredConstants as rc
from services.common.v1.methods.responseBody import createResponseBody
from services.helper import CustomJsonEncoder
from flask import current_app as app
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
#CBASEMgeTeQ03GKWtfTv7BEjs0EgAQ
#B%vA2wFdTrB%N8_V
#logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
class Transactions:
	def __init__(self):
		logger.info("Creating Transaction Class")

		# #initiaize square
		# squareconnect.configuration.access_token = rc.SQUARE_ACCESS_TOKEN
		# if(rc.EMAIL_ENV  == "dev"):
		# 	app.config.update(
		# 		MAIL_SERVER = 'smtp.gmail.com',
		# 		MAIL_PORT = 465,
		# 		MAIL_USE_SSL = True,
		# 		MAIL_USERNAME = rc.EMAIL_USER,
		# 		MAIL_PASSWORD = rc.EMAIL_PASSWORD,
		# 	)
		# self.mail =  Mail(app)
		self.db = dynamo
		# self.getLocationId()
		# self.transaction = TransactionsApi()
		
	def saveToDataBase(self,data):
		# orderStatus = {
		# 	"shipped": False,
		# 	"delivered":False,
		# 	"trackingNumber": None,
		# 	"dateShipped":None,
		# 	"deliveryCompany":None,
		# 	"cancelled":False,
		# 	"processing":True
		# }
		orderNumber = self.createOrderNumber()
		print(data)
		data["event_id"] = orderNumber
		data["dateAdded"] = datetime.datetime.now().isoformat()
		try:
			logger.info("Adding order number " + orderNumber +" to the database")
			#need another field for cartID
			self.db.tables[rc.tableToUse].put_item(Item = data)
			# logger.info("Order number " + orderNumber +" was successfully added to the database.")
			response = self.db.tables[rc.tableToUse].get_item(Key={'event_id': orderNumber})["Item"]
			
			# if error:
			# 	return createResponseBody(500, error= "Error sending your email. Order was placed. Please contact info@cocolevio.com. Your order number " +orderNumber),500
			# else:
			# 	logger.info("Success creating and charging order number " + orderNumber)
			# 	return response
		except Exception as e:
			logger.error("Error  when adding the transaction to the account. {}".format(e))
			return createResponseBody(500, error= "Exception when adding the transaction to the account: {}".format(e)),500
			
	def createOrderNumber(self):
		orderNumber = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
		try:
		    result = self.db.tables[rc.tableToUse].get_item(Key={'event_id': orderNumber})
		    if 'Item' not in result:
		    	logger.info("Creted order number " + orderNumber)
		    	return orderNumber #its not already used
		    else:
		        return self.createOrderNumber()
		except Exception as e:
			logger.error("There was a problem creating the account number. {}".format(e))
			return createResponseBody(500, error="There was a problem creating the account number: {}".format(e))

	
	#make this more efficient, as of right now, it will work bc there arent that many orders... imple
	def getAll(self):
		print("HEre inside")
		try:
			orders = self.db.tables[rc.tableToUse].scan()
			if (orders["Count"] > 0):

				logger.info("Success getting all items from table")
				#need a better way to change from Decimal to number....
				# for order in orders["Items"]:
				# 	self.changeObject(order)
				return orders["Items"]
			else:
				return createResponseBody(404, error= "There are not any orders"),404 
		except Exception as e:
			logger.error("Error 400, Could not connect to database. {}".format(e))
			print(e)
			return createResponseBody(400, error= "Could not connect to database. {}".format(e)),400 

