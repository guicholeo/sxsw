import cgi
from flask import Flask,request, abort, redirect,render_template
from flask_restplus import abort, Resource, Namespace
from services.extensions import logger
from services.config import RequiredConstants as rc

from services.common.v1.resources.transactions.Transactions import Transactions
# from services.common.v1.schemas.refund import refund_schema
api = sxsw_events = Namespace('sxsw', 'SXSW events')


# Setting the schemas used by this resource
# api.models[refund_schema.name] = refund_schema

#create a charge for the customer, need sqid and amount 

@api.route('/all')
class GetAllOrders(Resource):

	def get(self):
		logger.info("getOrderAll get route was called")
		# orderNumber = request.data.decode("utf-8")
		tran = Transactions()
		return tran.getAll()

@api.route('/add')
class AddEvent(Resource):

	def post(self):
		logger.info("getOrderAll get route was called")
		data = request.json
		# orderNumber = request.data.decode("utf-8")
		tran = Transactions()
		return tran.saveToDataBase(data)



