from flask_restplus import Api
from services.blueprints import common_blueprint
# from services.common.v1.resources.AccountResource import ns_account
from services.common.v1.resources.transactions.TransactionResource import sxsw_events
#from services.common.v1.resources.AuthResource import user
api = Api(common_blueprint,version='1.0', title='SXSW API', description='SXSW API')

# Configuring prefix for each of the namespace is optional, but recommended
api.add_namespace(sxsw_events, path='')
# api.add_namespace(user, path='/user')

#api = Api(common_blueprint, doc='/doc/')

# api.add_namespace(ns_account, path='/account')

# If you don't specify the path, it will use the Namespace's name as prefix
# api.add_namespace(ns_student)

# You can also configure each resource individually
# from services.common.v1.resources.student import StudentAccount
# api.add_resource(StudentAccount, '/students/<string:eid>')