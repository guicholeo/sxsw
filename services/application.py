# [START imports]
from flask import Flask
from services.config import RequiredConstants
from services.blueprints import all_blueprints
from services.extensions import dynamo,mail,logger
import logging
from logging.handlers import RotatingFileHandler
from flask_dynamo import Dynamo
# from flask.ext.cors import CORS
#from flask_bcrypt import Bcrypt
from flask_cors import CORS, cross_origin
from importlib import import_module
# [END imports]

def create_app(config_obj=None, **kwargs):
    print("HERE");
    configureLogger()
    app = Flask(__name__)
    """
    Creating a Flask application.
    :param config_obj: a Config object from config.py
    :param kwargs:  any other arguments will be passed to Flask()
    :return: an instantiated Flask app
    """
    register_blueprints(app)
    #configure Dynamo from tests
    if config_obj:
        app.config.from_object(config_obj)
    else:
        logger.info("Configuring DynamoDB")
        configureDynamo(app)
        logger.info("Finished Configuring DynamoDB")

    # logger.info("Configuring Mail")
    # configureMail(app)
    # logger.info("Finished Configuring Mail")
    # app = Flask(__name__)

    # TODO: Secure
    # temporary config
    #app.secret_key = "It's a secret"

    

    #configureDynamo(app)

    # Cross Origin Server
    cors = CORS(app, resources={r"/*": {"origins": "*"}})
    return app

def configureLogger():
    logFilePath = "./logs/order.log"
    logger.setLevel(logging.DEBUG)

    #rotating handler
    handler = RotatingFileHandler(logFilePath, maxBytes= 5242880, backupCount=20)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.info("Finished configuring logger")

def configureMail(app):
    mail_settings = {
        "MAIL_SERVER": 'smtp.gmail.com',
        "MAIL_PORT": 465,
        "MAIL_USE_TLS": False,
        "MAIL_USE_SSL": True,
        "MAIL_USERNAME": RequiredConstants.EMAIL_USER,
        "MAIL_PASSWORD": RequiredConstants.EMAIL_PASSWORD
    }

    app.config.update(mail_settings)
    mail.init_app(app)

def configureDynamo(app):

    app.config['DYNAMO_TABLES'] = [
        {
            'TableName': 'events',
            'KeySchema': [{'AttributeName':'event_id', 'KeyType':'HASH'}],
            'AttributeDefinitions': [ {'AttributeName':'event_id', 'AttributeType':'S'}],
            'ProvisionedThroughput': {'ReadCapacityUnits':5, 'WriteCapacityUnits':5}
        }
    ]
    
    app.config['DYNAMO_ENABLE_LOCAL'] = RequiredConstants.DYNAMO_ENABLE_LOCAL
    app.config['DYNAMO_LOCAL_HOST'] = RequiredConstants.DYNAMO_LOCAL_HOST
    app.config['DYNAMO_LOCAL_PORT'] = RequiredConstants.DYNAMO_LOCAL_PORT
    # app.config['AWS_REGION'] = RequiredConstants.AWS_REGION
    app.config['AWS_ACCESS_KEY_ID'] = RequiredConstants.AWS_ACCESS_KEY_ID
    app.config['AWS_SECRET_ACCESS_KEY'] = RequiredConstants.AWS_SECRET_ACCESS_KEY
    

    #Initialize Dynamo
    dynamo.init_app(app)
    with app.app_context():
        dynamo.create_all()

    # shell context for flask cli
    #app.shell_context_processor({'app': app, 'db': dynamo})

def register_blueprints(app):
    # TODO: Register blueprints
    logger.info("Starting to register blueprints")
    for bp in all_blueprints:
        import_module(bp.import_name)
        app.register_blueprint(bp)
    logger.info("Finished registering blueprints")