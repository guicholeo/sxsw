# [START imports]
from flask_dynamo import *
import os
# [END imports]


class RequiredConstants(object):
    """
    Constants used throughout the application.
    All hard coded settings/data that are not actual/official configuration options
    for Flask, MongoEngine, or any other extensions/middleware goes here.
    """
    ADMINS = ['admin@cocolevio']
    ENVIRONMENT = property(lambda self: self.__class__.__name__)
    DM_MODEL_IMPORTS = ['Customer', 'Subscription', 'Contacts', 'User']

    # TODO: Get AWS Credentials!
    #docker.for.mac.host.internal
    
    DYNAMO_ENABLE_LOCAL = os.getenv('DYNAMO_ENABLE_LOCAL', 'True')  == 'True'
    DYNAMO_LOCAL_HOST = os.getenv('DYNAMO_LOCAL_HOST', 'localhost')
    DYNAMO_LOCAL_PORT = int(os.getenv('DYNAMO_LOCAL_PORT', '8000'))
    AWS_REGION=os.getenv('AWS_REGION', 'us-east-1')
    AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY', "default")
    AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID', "default")
    SQUARE_ACCESS_TOKEN = os.getenv('SQUARE_ACCESS_TOKEN', "sandbox-sq0atb-YE1pa1XtTc8jd9TeSwPqjg")
    tableToUse = 'events'
    EMAIL_USER = os.getenv('EMAIL_USER', "cocoleviotest@gmail.com")
    EMAIL_PASSWORD = os.getenv('EMAIL_PASSWORD', "jtutbeicjxcoriak")
    EMAIL_ENV = os.getenv("EMAIL_ENV",'dev')
    RELOADER = os.getenv("RELOADER", 'True') == 'True'