# [START imports]
from services.extensions import dynamo as db
# [END imports]

#is this even used?
class Customer(db.Document):

    class Contacts(db.EmbeddedDocument):
        firstName = db.StringField()
        lastName = db.StringField()
        email = db.EmailField()
        phone = db.StringField()
        phoneExt = db.StringField()
        type = db.StringField()

    class Subscription(db.EmbeddedDocument):
        plan = db.StringField()
        startDate = db.DateTimeField()
        endDate = db.DateTimeField()
        autoRenew = db.BooleanField()
        planRate = db.StringField()
        planDiscount = db.StringField()
        discountEndDate = db.DateTimeField()

    class Address(db.EmbeddedDocument):
        address1 = db.StringField()
        address2 = db.StringField()
        address3 = db.StringField()
        city = db.StringField()
        state = db.StringField()
        postalCode = db.StringField()
        country = db.StringField()


    # Customer Information
    email = db.StringField()
    accountNumber = db.StringField()
    companyName = db.StringField()
    password = db.StringField()
    type = db.StringField()
    active = db.StringField()
    
    # Contacts (Technical, Billing, etc.)
    contacts = db.ListField(db.EmbeddedDocumentField(Contacts))
    
    #Address of the company
    address = db.ListField(db.EmbeddedDocumentField(Address))
    # Subscription
    subscription = db.EmbeddedDocumentField(Subscription)

    def __repr__(self):
        return '<Account %r>' % self.email