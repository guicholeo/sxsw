import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpRequest, HttpHeaders  } from "@angular/common/http";
import { Observable } from 'rxjs/Rx';
import {RequestOptions} from '@angular/http';

// import 'services/common/v1/api.py';

@Injectable()
export class Order {

    constructor(private http: HttpClient) {}

    //order

    updateOrder(dict:any,orderNumber:any): Observable<any> { //*
        return this.http.post("/api-order/order/update/"+orderNumber,dict);
    }

    checkout(dict:any): Observable<any> {
        return this.http.post("/api-order/order/checkout",dict);
    }

    refund(orderNumber:any): Observable<any>{ //*
        return this.http.post('/api-order/order/refund/'+orderNumber,null);
    }

    getOrder(orderNumber:any): Observable<any>{
        return this.http.get('/api-order/order/order/'+orderNumber);
    }

    getAllOrders(): Observable<any>{ //*
        return this.http.get('/api-order/order/order/all');
    }

    upload(form:any): Observable<any> {
        return this.http.get('/api-order/order/order/upload');
    }


//cart

    getCart(id:any): Observable<any>{
        return this.http.get('/api-cart/v1/cart/'+id)
    }//

    //catalog

    getAllItems(): Observable<any>{
        return this.http.get('/api-order/sxsw/all')

    }//
    updateItem(product_code:any,itemInfo:any): Observable<any>{ //*
        return this.http.post('/api-catalog/v1/item/update/'+product_code,itemInfo);
    }

    updateItemStock(product_code:any,itemInfo:any): Observable<any>{ //*
        return this.http.post('/api-catalog/v1/item/updateStock/'+product_code,itemInfo);
    }

    getCatalogItem(id:any): Observable<any>{
        return this.http.get('/api-catalog/v1/item/'+id);
    }
    //

    deleteItem(product_code:any): Observable<any>{ //*
        return this.http.delete('/api-catalog/v1/item/delete/'+product_code);
    }

    addItem(item:any): Observable<any>{ //*
        let headers = new HttpHeaders();
        // * In Angular 5, including the header Content-Type can invalidate your request 
        //headers.append('Content-Type', 'multipart/form-data');
        return this.http.post('/api-catalog/v1/item/create',item);
    }

    updateShirt(product_code:any,blob:any): Observable<any>{ //*
        return this.http.post('/api-catalog/v1/item/updateShirt/'+product_code,blob);
    }
}
