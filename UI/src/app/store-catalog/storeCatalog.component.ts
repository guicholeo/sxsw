// import {OrderInformationService} from '../order-confirmation/orderInformation.service';
import {Component,NgModule,OnInit,ChangeDetectorRef,OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Order} from "../order.service";
import * as Cookies from 'es-cookie';
// import { CatalogItemDialog } from '../catalog-item-dialog/catalogItemDialog.component';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';


@Component({
  selector: 'store-catalog',
  templateUrl: './storeCatalog.component.html',
  styleUrls: ['./storeCatalog.component.css']
})


// class Item{
//   name: String;
//   quantity:String;
//   price:String;
//   product_sku:String;
//   sku_code:String;
//   image:String;
// }


export class StoreCatalog implements OnInit,OnDestroy {
	catalogItems:any
	modal:any;
	item_name:string;
	quantity:string;
	price:string;
	product_code:string;
	sku_code:string;
	image_name:string;
	itemInfo:any;
	newItem:boolean;
	selectedFile:File;
	form:FormData;
	shirtSize:any;
	addedShirt:boolean;
	totalShirts = 0;
	allShirts:any;
	fileAdded = false;
	dataTable:any;

	allItems:any


	constructor(

		private orderService:Order,
		private router:Router,
		private chRef: ChangeDetectorRef
		) {}

	ngOnInit(){
		this.itemInfo= {};
		this.item_name = "";
		this.quantity = "";
		this.price = "";
		this.product_code = "";
		this.sku_code = "";
		this.image_name = "";
		this.form = new FormData();
		console.log("about to to call all")
		this.getAll();
		this.addedShirt = false;
		console.log("HEHHHEHE2")
		//saras code
		// this.modal = this.bsModalService.onHide.subscribe(reason =>{
		// 	console.log("inside the subscribe catalogs")
		// 	console.log("destroying")
			
		// 	this.getAll(); //should only be when there is a change...
		// });
	}



	getAll(){
		console.log("GETTING ALL")
		this.orderService.getAllItems().subscribe(res => {
			//
			this.catalogItems = res;
			this.chRef.detectChanges();
			const table: any = $('.catalogTable');
			this.dataTable = table.DataTable({
              "order":[[1,"desc"]]
            });

		}, (err) => {
		    
		   
		      		
        }); 
	}

	convertDate(date){
		return new Date(date).toDateString();
	}


	ngOnDestroy(){
		console.log("unsubscribe")
		this.modal.unsubscribe()


	}
}