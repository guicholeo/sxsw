import { Component, OnInit } from '@angular/core';
import { HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	username:String;
	password:String;
  constructor(
  	private http:HttpClient) { }

  ngOnInit() {

  }

  login(){
  	var info = {
  		"email":this.username,
  		"password":this.password
  	}


  	console.log(info)
  	this.http.post('/admin/login',info,{observe:"response"}).subscribe(res => {
  		console.log(res)
  		if(res.status === 200){
  			 window.location.href = '/admin';
  		}else{
        alert("Wrong email or password. Please try again.")
      }
  	})

  }

}
