import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClarityModule } from "@clr/angular";
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
// import { MatDialogModule } from "@angular/material/dialog";
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import {MatButtonToggleModule} from '@angular/material/button-toggle';
// import {MatFormFieldModule} from '@angular/material/form-field';
// import {MatSelectModule} from '@angular/material/select';
// import {MatInputModule} from '@angular/material/input';
// import { ModalModule } from 'ngx-bootstrap/modal';


import { Order} from "./order.service";

import { StoreCatalog } from './store-catalog/storeCatalog.component'



import {AppComponent} from './app.component';
import { AppRoutingModule } from "./app-routing.module";
import { LoginComponent } from './login/login.component';
// import { ProductsComponent } from './products/products.component'

@NgModule({
  declarations: [
    AppComponent,
    StoreCatalog,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    ClarityModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  entryComponents: [],
  providers: [Order],
  bootstrap: [AppComponent]
})
export class AppModule { }
