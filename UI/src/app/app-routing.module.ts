import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { StoreCatalog } from './store-catalog/storeCatalog.component'

import { LoginComponent } from './login/login.component';
//import { ProductsComponent } from './products/products.component';

const routes: Routes = [
   {path: '', component:  StoreCatalog , data: {title: 'SXSW'}},
    //{path: '**', component:  OrderConfirmation , data: {title: 'Home | Dell Medical'}},
    //make this path a module for the cart
    //{path: "login",component: LoginComponent, data: {title: 'Home | Dell Medical'}}
];


@NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class AppRoutingModule { }
  
  