from services.application import create_app
from flask import Flask, url_for,render_template
from services.config import RequiredConstants
import os
app = create_app()

@app.route('/')
def hello_world():
       return render_template('index.html')

if __name__=="__main__":
	print("HERE INSINDE NAME")
	print(RequiredConstants.RELOADER)
	app.run(debug=True, host='0.0.0.0',port=int(os.getenv('PY_PORT', '5200')), use_reloader=RequiredConstants.RELOADER)
