# Stage 1
FROM node:alpine AS build-env

# Environmental Variables
ARG base_href=/sxsw
ENV base_href=$base_href
ARG stage


LABEL maintainer="info@cocolevio.com"

# Create app directory
WORKDIR /UI

# Install app dependencies
# Wildcard used to copy both package.json and package-lock.json
#COPY package*.json ./
COPY ./UI ./

# Dev install # Build Angular app
RUN npm install && $(npm bin)/ng build --prod --environment=$stage --build-optimizer --deploy-url $base_href --base-href $base_href


# Final Stage
# Small linux images great justice
FROM node:alpine

# Our working directory
WORKDIR /app

RUN apk add --update --no-cache libffi-dev python3 python3-dev libc-dev gcc openssl-dev && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache

# Copying over src files we need
COPY package*.json manage.py requirements.txt ./
COPY services ./services
COPY logs ./logs

# Copy over Angular App that was built in first stage
COPY --from=build-env /UI/dist /app/UI/dist
COPY --from=build-env /UI/package*.json /UI/server.js /app/UI/

#Production install
RUN npm run install-prod

# Port to expose and command to start the server.
EXPOSE 8200 5200
CMD ["npm", "start"]
