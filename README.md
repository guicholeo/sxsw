# Python Flask Service

## Structure
```
.
├── services - Flask-restplus application  
│   ├── common - Common is the name of your service (`common-service`) follows by a separate Python 
               module indicating the version of the service.
│   │   ├── v1 - REST API Versioning
│   │   │   ├── resources - Contains Flask-Restplus Resources objects that could be used by your 
                            Flask-Restplus Api object
│   │   │   │   ├── __init__.py
│   │   │   │   └── StudentAccount.py - an example of a Flask-Restplus Resource
│   │   │   ├── api.py Setting up the Flask-Restplus Api object by connecting it with a Flask Blueprint.
                       This could also contain any Api endpoints routing rules that you want to apply 
                       for each different resource (depned on your design choice).
                       Also sets up documentation routing.
│   │   │   └── __init__.py
│   │   ├── v2 - REST API Versioning
│   │   │   ├── resources
│   │   │   │   └── __init__.py
│   │   │   └── __init__.py
│   │   └── __init__.py
│   ├── core - Contains any commonly shared code that doesn't play any specific role into your RESTful API.
             For example, helper functions providing way to format strings, timedate, etc.
│   │   └── __init__.py
│   ├── models - Contains Data models, schemas and anything that could be used throughout your service application
│   │   ├── Course.py
│   │   ├── CourseSchedule.py
│   │   ├── __init__.py
│   │   ├── Person.py
│   │   └── Type.py
│   ├── application.py - Manage how your Flask app bundle different aspects of Flask configuration together.
                         Included (but not limited to) Flask Blueprints, Flask Configs, any other Flask-Extensions that need to be initialized.
│   ├── blueprints.py - Modulating different APIs or different versions of APIs that your service provide
│   ├── config.py - Flask Config object. This is the recommended route of configuring your application's config
│   ├── extensions.py  - Initialize, configure and setup any Flask Extensions that you might use (Redis, Celery, Mail, ...)
│   └── __init__.py
├── tests - Folder containing all unit tests, E2E tests, ...
├── bitbucket-pipelines.yml
├── Dockerfile
├── flask_service.iml
├── __init__.py
├── manage.py - Entry point of the service. Any kind of database setup, management or 
              parsing command line args should go in here. This is where you can set up other
              applications to interact with your service
├── README.md
├── requirements.txt
└── sonar-project.properties
```# Tipsy


#Docker Commands:

#When using Docker for Windows or Docker for Mac and insidie a container, use the following address to access localhost on the host machine.
host.docker.internal

#Build image
docker build -t tipsy-account .

#Run container based off image
docker run -p 8300:8300 -p 5000:5000 --name tipsy-account-container tipsy-account

#SSH Into container using Windows Docker CE
winpty docker.exe exec -it tipsy-account-container /../bin/bash

#SSH into container using other OS (container is running alpine linux)
docker exec -it tipsy-account-container /bin/ash

#List all containers (only IDs)
docker ps -aq

#Stop all running containers
docker stop $(docker ps -aq)

#Remove all containers
docker rm $(docker ps -aq)

#Remove all images
docker rmi $(docker images -q)