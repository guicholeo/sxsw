from flask_testing import LiveServerTestCase
import unittest
from services.application import create_app
import requests


class ApplicationSpec(LiveServerTestCase):

    def create_app(self):
        app = create_app()
        app.config['TESTING'] = True
        # Default port is 5000
        app.config['LIVESERVER_PORT'] = 5000
        # Default timeout is 5 seconds
        app.config['LIVESERVER_TIMEOUT'] = 10
        return app

    def test_server_is_up_and_running(self):
        response = requests.get(self.get_server_url()+"/common/v1/doc/")
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()