# import unittest
# import json
# from flask import Flask
# from services.application import create_app
# #from services.application import app, register_blueprints
# from services.config import RequiredConstants as rc
# from services.extensions import dynamo
# from flask_dynamo import Dynamo
# from werkzeug.datastructures import MultiDict

# base_url = '/tipsy/customer'
# square_id = ''
# #register_blueprints(app)
# class CustomerTests(unittest.TestCase):

#     def setUp(self):
#         #class to pass
#         class TestConfig:
#             TESTING = True

#         rc.tableToUse = 'customerTestingDataBase'
#         app = create_app(config_obj=TestConfig())
#         app.config['DYNAMO_TABLES'] = [
#             {
#                 'TableName': rc.tableToUse,
#                 'KeySchema': [dict(AttributeName='account_number', KeyType='HASH')],
#                 'AttributeDefinitions': [dict(AttributeName='account_number', AttributeType='S')],
#                 'ProvisionedThroughput': dict(ReadCapacityUnits=5, WriteCapacityUnits=5)
#             }
#         ]
#         #app.config['DYNAMO_ENABLE_LOCAL'] = True
#         #app.config['DYNAMO_LOCAL_HOST'] = 'localhost'
#         #app.config['DYNAMO_LOCAL_PORT'] = 8000
#         app.config['AWS_ACCESS_KEY_ID'] = rc.AWS_ACCESS_KEY_ID
#         app.config['AWS_SECRET_ACCESS_KEY'] = rc.AWS_SECRET_ACCESS_KEY
#         dynamo.init_app(app)
#         with app.app_context():
#             dynamo.create_all()
#         rc.sq_id = square_id
#         self.app = app
#         self.client = app.test_client()
#         self.db = dynamo

#         #changing the table name for testing
        

#     def tearDown(self):
#         # try:
#         #     with self.app.app_context():
#         #         self.db.destroy_all()
#         # except Exception:
#         #     with self.app.app_context():
#         #         self.db.destroy_all()
#         pass


#     ############################
#     '''CREATE CUSTOMERS TESTS'''
#     ############################

#     def test_createTestCustomer(self):
#         ''' Testing creating customer with correct information '''
#         create = '/create'
#         response = self.client.post(
#             base_url + create,
#             data=json.dumps(dict(
#                 companyName="TestCompansy",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 type="Restaurant",
#                 customerInfomation=dict(
#                     firstName="Leo",
#                     lastName="Test",
#                     email="leo@test.com",
#                     phone="111111111",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 contacts=[dict(
#                     firstName="TestFirstName",
#                     lastName="TestLastName",
#                     email="email@test.com",
#                     phone="123-456-7890",
#                     phoneExt="123",
#                     type="Technical"
#                 )],
#                 subscription=[dict(
#                     Plan="planTest",
#                     StartDate="2012-12-20T03:00:00.000Z",
#                     EndDate="2013-12-20T03:00:00.000Z",
#                     AutoRenew=True,
#                     PlanRate="9999",
#                     PlanDiscount="9999",
#                     DiscountEndDate="2013-12-20T03:00:00.000Z",
#                     ProductType="Tipsy",
#                 )]
#             )),
#             content_type='application/json'
#         )
#         #get_data will get repricated later, be aware
#         data = json.loads(response.get_data(as_text=True))
#         global account_number
#         account_number = data["Item"]["account_number"]

#         self.assertEqual(response.status_code, 200)

#     def test_createTestCustomerMoreInfo(self):
#         ''' Testing creating customer with more information '''
#         create = '/create'
#         response = self.client.post(
#             base_url + create,
#             data=json.dumps(dict(
#                 companyName="TestCompany",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 type="Restaurant",
#                 extraInfo="TestExtraInfo",
#                 customerInfomation=dict(
#                     firstName="Leo",
#                     lastName="Test",
#                     email="leo@test.com",
#                     phone="111111111",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 contacts=dict(
#                     firstName="TestFirstName",
#                     lastName="TestLastName",
#                     email="email@test.com",
#                     phone="123-456-7890",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 subscription=[dict(
#                     Plan="planTest",
#                     StartDate="2012-12-20T03:00:00.000Z",
#                     EndDate="2013-12-20T03:00:00.000Z",
#                     AutoRenew=True,
#                     PlanRate="9999",
#                     PlanDiscount="9999",
#                     DiscountEndDate="2013-12-20T03:00:00.000Z"
#                 )]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 500)

#     def test_createTestCustomerLessInfo(self):
#         ''' Testing creating customer with less information '''
#         create = '/create'
#         response = self.client.post(
#             base_url + create,
#             data=json.dumps(dict(
#                 companyName="TestCompany",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 type="Restaurant"
#             )),
#             content_type='application/json'
#         )
#         #get_data will get repricated later, be aware

#         self.assertEqual(response.status_code, 500)

#     def createTestCustomerBadInfo(self):
#         ''' Testing creating customer with bad information '''
#         create = '/create'
#         response = self.client.post(
#             base_url + create,
#             data=json.dumps(dict(
#                 companyName="TestCompany",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 type="Restaurant",
#                 contacts=dict(
#                     firstName="TestFirstName",
#                     lastName="TestLastName",
#                     email="email@test.com",
#                     phone="123-456-7890",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 subscription=dict(
#                     plan="planTest",
#                     StartDate="startDate",
#                     EndDate="endDate",
#                     AutoRenew="AutoRenew",
#                     PlanRate="9999",
#                     PlanDiscount="9999",
#                     DiscountEndDate="2013-12-20T03:00:00.000Z"
#                 )
#             )),
#             content_type='application/json'
#         )
#         #get_data will get repricated later, be aware
#         self.assertEqual(response.status_code, 500)


#     #########################
#     '''GET CUSTOMERS TESTS'''
#     #########################

#     def test_get_customer(self):
#         ''' Testing getting the customer information with the correct id '''
#         response = self.client.get(base_url +"/"+ account_number, follow_redirects=True)
#         print(response.data)
#         self.assertEqual(response.status_code, 200)

#     def test_get_customer2(self):
#         ''' Testing getting the information with a non id'''
#         cust_id = "/" + account_number
#         response = self.client.get(base_url + cust_id+"-fads$#$%#", follow_redirects=True)
#         self.assertEqual(response.status_code, 404)

#     def test_get_customer3(self):
#         ''' Testing getting the information with no id'''
#         response = self.client.get(base_url +"/", follow_redirects=True)
#         self.assertEqual(response.status_code, 404)



#     # ############################
#     # '''UPDATE CUSTOMERS TESTS'''
#     # ############################

#     def test_update_customer(self):
#         ''' Testing updating customer. '''
#         #with extra info, maybe we add more later, modify this
#         update = '/update/' + account_number
#         response = self.client.post(
#             base_url + update,
#             data=json.dumps(dict(
#                 companyName="UpdateTestCompany",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 type="Restaurant",
#                 customerInfomation=dict(
#                     firstName="Leo",
#                     lastName="Test",
#                     email="leo@test.com",
#                     phone="111111111",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 contacts=[dict(
#                     firstName="UpdateTestFirstName",
#                     lastName="UpdateTestLastName",
#                     email="updateemail@test.com",
#                     phone="Update123-456-7890",
#                     phoneExt="Update123",
#                     type="UpdateTechnical"
#                 )],
#                 subscription=[dict(
#                     plan="UpdatePlanTest",
#                     StartDate="2014-12-20T03:00:00.000Z",
#                     EndDate="2015-12-20T03:00:00.000Z",
#                     AutoRenew=False,
#                     PlanRate="89999",
#                     PlanDiscount="89999",
#                     DiscountEndDate="2014-12-20T03:00:00.000Z",
#                     ProductType="Tipsy",
#                 )]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 200)

#     def test_update_customer2(self):
#         ''' Testing updating customer with wrong id, right parameters '''
#         #with extra info, maybe we add more later, modify this
#         update = '/update/'+account_number+"124897"
#         response = self.client.post(
#             base_url + update,
#             data=json.dumps(dict(
#                 companyName="UpdateTestCompany",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 type="Restaurant",
#                 customerInfomation=dict(
#                     firstName="Leo",
#                     lastName="Test",
#                     email="leo@test.com",
#                     phone="111111111",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 contacts=[dict(
#                     firstName="UpdateTestFirstName",
#                     lastName="UpdateTestLastName",
#                     email="updateemail@test.com",
#                     phone="Update123-456-7890",
#                     phoneExt="Update123",
#                     type="UpdateTechnical"
#                 )],
#                 subscription=[dict(
#                     plan="UpdatePlanTest",
#                     StartDate="2014-12-20T03:00:00.000Z",
#                     EndDate="2015-12-20T03:00:00.000Z",
#                     AutoRenew=False,
#                     PlanRate="89999",
#                     PlanDiscount="89999",
#                     DiscountEndDate="2014-12-20T03:00:00.000Z",
#                     ProductType="Tipsy",
#                 )]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 500)

#     def test_update_customer3(self):
#         ''' Testing updating customer with right id,not all parameters '''
#         update = '/update/'+account_number
#         response = self.client.post(
#             base_url + update,
#             data=json.dumps(dict(
#                 companyName="UpdateTestCompany",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 type="Restaurant",
#                 customerInfomation=dict(
#                     firstName="Leo",
#                     lastName="Test",
#                     email="leo@test.com",
#                     phone="111111111",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 type="UpdateRestaurant"
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 500)

#     def test_update_customer4(self):
#         ''' Testing updating customer with no ID '''
#         update = '/update/'
#         response = self.client.post(
#             base_url + update,
#             data=json.dumps(dict(
#                 companyName="UpdateTestCompany",
#                 address1="Update1234 Lane",
#                 address2="Update5432 Street",
#                 city="UpdateCity",
#                 state="UpdateState",
#                 postCode="Update77777",
#                 country="UpdateUSA",
#                 type="UpdateRestaurant"
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 404)

#     def test_update_customer5(self):
#         ''' Testing updating customer not correct parameters '''
#         #with extra info, maybe we add more later, modify this
#         update = '/update/' + account_number
#         response = self.client.post(
#             base_url + update,
#             data=json.dumps(dict(
#                 companyName="UpdateTestCompany",
#                 address=dict(
#                     address1="1234 Lane",
#                     address2="5432 Street",
#                     city="City",
#                     state="State",
#                     postCode="77777",
#                     country="US"
#                 ),
#                 customerInfomation=dict(
#                     firstName="Leo",
#                     lastName="Test",
#                     email="leo@test.com",
#                     phone="111111111",
#                     phoneExt="123",
#                     type="Technical"
#                 ),
#                 type=True
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 400)

#     def test_update_customer6(self):
#         ''' Testing updating customer with no data '''
#         #with extra info, maybe we add more later, modify this
#         update = '/update/'+account_number
#         response = self.client.post(
#             base_url + update,
#             content_type='application/json'
#             )

#         self.assertEqual(response.status_code, 400)

#     def test_update_subscription1(self):
#         '''
#         Testing updating subscription
#         '''
#         subscription = '/subscription/'+account_number
#         response = self.client.post(
#             base_url + subscription,
#             data=json.dumps(dict(
#                 subscription=[dict(
#                     plan="UpdatePlanTest",
#                     StartDate="2014-12-20T03:00:00.000Z",
#                     EndDate="2015-12-20T03:00:00.000Z",
#                     AutoRenew=False,
#                     PlanRate="89999",
#                     PlanDiscount="89999",
#                     DiscountEndDate="2014-12-20T03:00:00.000Z",
#                     ProductType="Tipsy",
#                 )]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 200)

#     def test_update_subscription2(self):
#         ''' Testing update subscription wrong user '''
#         subscription = '/subscription/'+account_number+"5325"
#         response = self.client.post(
#             base_url + subscription,
#             data=json.dumps(dict(
#                 subscription=[dict(
#                     plan="UpdatePlanTest",
#                     StartDate="2014-12-20T03:00:00.000Z",
#                     EndDate="2015-12-20T03:00:00.000Z",
#                     AutoRenew=False,
#                     PlanRate="89999",
#                     PlanDiscount="89999",
#                     DiscountEndDate="2014-12-20T03:00:00.000Z",
#                     ProductType="Tipsy",
#                 )]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 404)

#     def test_update_subscription3(self):
#         ''' Testing update subscription missing variables '''
#         subscription = '/subscription/'+account_number+"5325"
#         response = self.client.post(
#             base_url + subscription,
#             data=json.dumps(dict(
#                 subscription=[dict(
#                     plan="UpdatePlanTest",
#                     StartDate="2014-12-20T03:00:00.000Z",
#                     EndDate="2015-12-20T03:00:00.000Z",
#                     AutoRenew=False
#                 )]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 500)

#     def test_update_subscription4(self):
#         ''' Testing update subscription extra variables '''
#         subscription = '/subscription/'+account_number+"5325"
#         response = self.client.post(
#             base_url + subscription,
#             data=json.dumps(dict(
#                 subscription=[dict(
#                     plan="UpdatePlanTest",
#                     StartDate="2014-12-20T03:00:00.000Z",
#                     EndDate="2015-12-20T03:00:00.000Z",
#                     AutoRenew=False,
#                     PlanRate="89999",
#                     PlanDiscount="89999",
#                     DiscountEndDate="2014-12-20T03:00:00.000Z",
#                     ProductType="Tipsy",
#                     ExtraVariable="Extra"
#                 )]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 500)
#     def test_update_subscription5(self):
#         ''' Testing update subscription empty daya '''
#         subscription = '/subscription/'+account_number+"5325"
#         response = self.client.post(
#             base_url + subscription,
#             data=json.dumps(dict(
#                 subscription=[dict()]
#             )),
#             content_type='application/json'
#         )

#         self.assertEqual(response.status_code, 500)

#     ############################
#     '''Create Credit Card Tests'''
#     ############################
#     #CANT NOT BE TESTED EVEN WITH HARD CODED NONCE, BECAUSE ONCE USED, THEY ARE USELESS, AND CANT BE REUSED
#     #Hard coded nonce
#     #CBASEBBrT619zrSkwixi9RoxHNggAQ
#     #CBASENm5L66c5K2Z2AftxPWUmCAgAQ



#     # def test_create_credit_card1(self):
#     #     ''' Testing creating card with nonce  '''
#     #     #predetermined nonce, it can not be generated outside the square form CBASEB9MAXXunUgrE4RnUbQSwkYgAQ
#     #     createCard = '/createCard/'+account_number
#     #     formToSend = MultiDict([
#     #         ('nonce', 'CBASEKacIyPrHu8zZW3sVryICSAgAQ'),
#     #         ('address1', '1100 Warrington Dr.'),
#     #         ('address2', None),
#     #         ('address3', None),
#     #         ('city', 'Austin'),
#     #         ('state', 'Tx'),
#     #         ('postal_code', '78753'),
#     #         ('country', 'US')
#     #         ])
#     #     response = self.client.post(
#     #         base_url + createCard,
#     #         data=formToSend,
#     #         content_type='multipart/form-data'
#     #     )
#     #     self.assertEqual(response.status_code, 200)

#     # def test_create_credit_card2(self):
#     #     ''' Testing creating card with wrong username  '''
#     #     #Nonce doesnt matter, bc testing other stuff
#     #     createCard = '/createCard/'+account_number+'dsfsd'
#     #     formToSend = MultiDict([
#     #         ('nonce', 'CBASEB9MAXXunUgrE4RnUbQSwkYgAQ'),
#     #         ('address1', '1101 Warrington Dr.'),
#     #         ('address2', None),
#     #         ('address3', None),
#     #         ('city', 'Austin'),
#     #         ('state', 'Tx'),
#     #         ('postal_code', '78753'),
#     #         ('country', 'US')
#     #         ])
#     #     response = self.client.post(
#     #         base_url + createCard,
#     #         data=formToSend,
#     #         content_type='multipart/form-data'
#     #     )
#     #     self.assertEqual(response.status_code, 500)

#     # def test_create_credit_card3(self):
#     #     ''' Testing creating card with missing information  '''
#     #     #Nonce doesnt matter, bc testing other stuff
#     #     createCard = '/createCard/'+account_number
#     #     formToSend = MultiDict([
#     #         ('nonce', 'CBASEB9MAXXunUgrE4RnUbQSwkYgAQ'),
#     #         ('address1', '1122 Warrington Dr.'),
#     #         ('address2', None),
#     #         ('address3', None),
#     #         ('city', 'Austin'),
#     #         ('state', ''),
#     #         ('postal_code', '78753'),
#     #         ('country', '')
#     #         ])
#     #     response = self.client.post(
#     #         base_url + createCard,
#     #         data=formToSend,
#     #         content_type='multipart/form-data'
#     #     )
#     #     self.assertEqual(response.status_code, 500)

#     #Deleting the credit card by not passing a nonce, it needs to be executed after create_credit_card methods, thats
#     #why its the same name method . 
#     # def test_create_credit_card4_delete(self):
#     #     ''' Test delete card fom '''
#     #     url = "/deleteCard/"+account_number+"/0"
#     #     response = self.client.post(base_url+url, follow_redirects=True)
#     #     print(response.data)
#     #     self.assertEqual(response.status_code, 200)

#     # #again testing create card to reuse nonce
#     # def test_create_credit_card4(self):
#     #     ''' Testing creating card with nonce  '''
#     #     #predetermined nonce, it can not be generated outside the square form CBASENm5L66c5K2Z2AftxPWUmCAgAQ
#     #     createCard = '/createCard/'+account_number
#     #     formToSend = MultiDict([
#     #         ('nonce', 'CBASENm5L66c5K2Z2AftxPWUmCAgAQ'),
#     #         ('address1', '1100 Warrington Dr.'),
#     #         ('address2', None),
#     #         ('address3', None),
#     #         ('city', 'Austin'),
#     #         ('state', 'Tx'),
#     #         ('postal_code', '78753'),
#     #         ('country', 'US')
#     #         ])
#     #     response = self.client.post(
#     #         base_url + createCard,
#     #         data=formToSend,
#     #         content_type='multipart/form-data'
#     #     )
#     #     self.assertEqual(response.status_code, 200)

#     #     #Deleting the credit card by not passing a nonce
#     # def test_update_credit_card(self):
#     #     ''' Testing updating card with a new credit card Nonce. Deletes old index card'''
#     #     url = "deleteCard/"+account_number+"/0/"+"CBASEBBrT619zrSkwixi9RoxHNggAQ"
#     #     response = self.client.post(base_url +"/", follow_redirects=True)
#     #     self.assertEqual(response.status_code, 200)
#     # def test_delete_card(self):
#     #     ''' Testing deleting the card with wrong ID'''
#     #     url = "deleteCard/"+account_number+"/1"
#     #     response = self.client.post(base_url +"/", follow_redirects=True)
#     #     self.assertEqual(response.status_code, 500)

#     # def test_delete_card_index1(self):
#     #     ''' Testing deleting the card with right id and index 0. Deleting previous added card.'''
#     #     url = "deleteCard/"+account_number+"/0"
#     #     response = self.client.post(base_url +"/", follow_redirects=True)
#     #     self.assertEqual(response.status_code, 200)



#       ############################
#     '''DELETE CUSTOMERS TESTS'''
#     ############################

#     def test_z_delete_customer(self):
#             ''' Testing deleting with the in correct id '''
#             delete = "/delete/" + account_number
#             response = self.client.delete(base_url + delete, follow_redirects=True)
#             self.assertEqual(response.status_code, 200)
    

#     def test_z_delete_customer2(self):
#         ''' Testing deleting the customer with an incorrect id'''
#         delete = "/delete/" + account_number
#         response = self.client.delete(base_url + delete+"-fads$#$%#", follow_redirects=True)
#         self.assertEqual(response.status_code, 404)

#     def test_z_delete_customer3(self):
#         ''' Testing deleting the customer with no id'''
#         response = self.client.delete(base_url +"/", follow_redirects=True)
#         self.assertEqual(response.status_code, 404)


# if __name__ == '__main__':
#     unittest.main()



